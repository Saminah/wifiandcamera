package itsm.userservices;

import android.bluetooth.BluetoothClass;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btWifi,btBluetooth,btCamera,btExit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       Bindwiget();

    }

    void Bindwiget(){
        btWifi=findViewById(R.id.button_wifi);
        btBluetooth=findViewById(R.id.button_bluetooth);
        btCamera=findViewById(R.id.button_camera);
        btExit=findViewById(R.id.button_exit);

        btExit.setOnClickListener(V->{

            System.exit(0);
        });

        btWifi.setOnClickListener(V->{
            Intent wifiintent=new Intent(this,DeviceActivity.class);
            wifiintent.putExtra("deviceLabel","Wifi Device");
            startActivity(wifiintent);
        });

        btBluetooth.setOnClickListener(V->{
            Intent blintent=new Intent(this,DeviceActivity.class);
            blintent.putExtra("deviceLabel","Bluetoothe Device");
            startActivity(blintent);
        });


        btCamera.setOnClickListener(V->{
            Intent camintent=new Intent(this,CameraActivity.class);
            startActivity(camintent);
        });

    }
}
