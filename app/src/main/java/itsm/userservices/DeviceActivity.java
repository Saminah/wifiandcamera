package itsm.userservices;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class DeviceActivity extends AppCompatActivity {
    String getstr;
    TextView txtDevice;
    Button btbackD;
    Switch swcheck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        getstr = getIntent().getStringExtra("deviceLabel").toString();
        BindeWiget();

    }

    void BindeWiget() {

        txtDevice = findViewById(R.id.txt_device);
        txtDevice.setText(getstr);
        btbackD = findViewById(R.id.button_backD);
        btbackD.setOnClickListener(V -> {
            Intent backintent = new Intent(this, MainActivity.class);
            startActivity(backintent);
        });
        swcheck = findViewById(R.id.sw_device);
        swcheck.setOnClickListener(V -> {
            if (getstr.contentEquals("Wifi Device")) {
                if (swcheck.isChecked()) {
                    WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    wifi.setWifiEnabled(true);
                    Toast.makeText(this, "WIFI ON", Toast.LENGTH_LONG).show();
                }
                else {
                    WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                    wifi.setWifiEnabled(false);
                    Toast.makeText(this, "WIFI OFF", Toast.LENGTH_LONG).show();
                }
            }

            if (getstr.contentEquals( "Bluetoothe Device"))
            {
                BluetoothAdapter bluetoothdevice=BluetoothAdapter.getDefaultAdapter();
                if (swcheck.isChecked()) {
                    bluetoothdevice.enable();
                    Toast.makeText(this, "Bluetooth ON", Toast.LENGTH_LONG).show();
                }

                else {
                    bluetoothdevice.disable();
                    Toast.makeText(this, "Bluetooth OFF", Toast.LENGTH_LONG).show();
                }
            }
            });
    }
}
