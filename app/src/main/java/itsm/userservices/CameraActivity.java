package itsm.userservices;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

public class CameraActivity extends AppCompatActivity {
  Button btbackC,bttake;
  ImageView imgtake;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        bindwiget();
    }
    void bindwiget(){
       imgtake=findViewById(R.id.image_camera);
       btbackC =findViewById(R.id.button_backC);
       btbackC.setOnClickListener(V->{
            Intent backintent=new Intent(this,MainActivity.class);
            startActivity(backintent);
        });
       bttake=findViewById(R.id.button_take);
       bttake.setOnClickListener(V->{
           EasyImage.openCamera(this, 0);


       });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                Glide.with(CameraActivity.this).load(imageFile).into(imgtake);
            }
        });
    }
}
